/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  compiler: {
    // TODO: ver que tag vamos a usar para cypress asi los eliminamos del build
    // TODO: chequear si cuando se buildea para dev estos tags se eliminan
    reactRemoveProperties: { properties: ['^data-cy$'] },
  },
};

module.exports = nextConfig;
