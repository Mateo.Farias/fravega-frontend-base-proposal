import { IMockData } from '../../entities/MockData';

export default class MockVerification {
  private mockData: IMockData;

  constructor(mockData: IMockData) {
    console.log('mockData que llego', mockData);
    this.mockData = mockData;
  }

  validate(): boolean {
    return this.mockData.id.length > 0 && this.mockData.name.length > 0;
  }
}
