import MockVerification from './MockVerification';
import { IMockData } from '@/app/entities/MockData';

describe('MockVerification Interactor Tests', () => {
  test('returns true when properties length is > 0', () => {
    const mockData: IMockData = {id: 'mockId', name: 'mockName'};
    const mockVerificator = new MockVerification(mockData);
    const isValid = mockVerificator.validate();
    expect(isValid).toBe(true);
  });

  test('returns false when properties length is = 0', () => {
    const mockData: IMockData = {id: '', name: ''};
    const mockVerificator = new MockVerification(mockData);
    const isValid = mockVerificator.validate();
    expect(isValid).toBe(false);
  });
});
