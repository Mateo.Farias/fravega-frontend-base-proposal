import { IMockRepository } from '../../adapters/repositories/IMockRepository';
import { IMockData } from '../../entities/MockData';

export class MockRepository implements IMockRepository {
  getOne(id: string): IMockData {
    return {id: 'mock', name: 'mock'};
  }
}
