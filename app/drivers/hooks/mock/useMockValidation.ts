import { IMockData } from '../../../entities/MockData';
import MockVerification from '../../../interactors/mock/MockVerification';

const useMockValidation = (mockData: IMockData) => {
  const mockValidator = new MockVerification(mockData);

  return { mockValidator };
};

export default useMockValidation;
