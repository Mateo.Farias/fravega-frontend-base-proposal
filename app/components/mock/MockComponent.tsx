import useMockValidation from '../../drivers/hooks/mock/useMockValidation';

interface Props {
  id: string
  name: string
}

const MockComponent: React.FC<Props> = ({ id, name }: Props) => {
  console.log('llego', id, name);
  const { mockValidator } = useMockValidation({id, name});

  if (mockValidator.validate()) {
    return <div id={id}>{name}</div>;
  } else {
    return <div>error</div>;
  }
};

export default MockComponent;
