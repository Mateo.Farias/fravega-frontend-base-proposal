import React from 'react';
import { render, screen } from '@testing-library/react';
import MockComponent from './MockComponent';

describe('MockComponent tests', () => {
  it('renders properties', () => {
    render(<MockComponent id="a" name="mock" />);
    const name = screen.getByText('mock');
    expect(name).toBeDefined();
  });

  it('renders error', () => {
    render(<MockComponent id="" name="" />);
    const error = screen.getByText('error');
    expect(error).toBeDefined();
  });
});
