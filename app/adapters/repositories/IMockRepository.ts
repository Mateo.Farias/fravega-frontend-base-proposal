import { IMockData } from '../../entities/MockData';

export interface IMockRepository {
  getOne(id: string): IMockData
}
